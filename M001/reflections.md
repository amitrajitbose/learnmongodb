# M001
-----
## Introduction
#### Database, Collection, Documents
Databases are a bundle of collections. Collections are a bundles of similar types of documents. Each document has a namespace and can be accesses as `<databaseName>.<collectionName>`
#### Exploring Datasets
Use **home** button to return to the dashboard anytime. Use click and point UI to select a database from the left, then from the dropdown select a collection. You can view the schema summary, the fields, the data types, range and a small statistics of values in the tables and also explore other tabs like documents etc.
#### Documents : Scalar Value Type
Int32, Double, Date, String are some of the most common data type. Also, you will find undefined in the schema dashboard of a database which suggests null value or absense of data. For precise figures, decimals are preferred over Double. The term **Object** is also used interchangeably.
#### Datatype - Document
MongoDB supports nested documents. In other words two or more similar documents under one outer document. The inner documents have scalar types, and the outer document has data type as *document*. Nesting can happen for two or more levels depth as well.
#### Datatype - Array
Supports array fields containing a list of items whose index starts from zero.
#### Filtering Collections With Queries
We can select on a region in the schema graph summaries to apply the equality filters. Some fields have a range of values we can select a range and analyse to understand how they work.
Queries or filters are internally converted to JSON when we select one region.
*$* is an operator in MongoDB query language. Fo example: less than or equal to is represented by **$lte**. Similarly, for greater than we have **$gt**.
#### Geospatial Queries
To identify distances and points in a radius, etc. This is one of the most awesome and cool features of Compass. Suppose you have a map widget, and you want the details or basically you want your query to mark all the points in the radius of a point. You need to assume a centre, press **SHIFT** key and drag the mouse to decide a radius. Then, you need to **Analyze**. The circle selected will be marked and highlighted.
#### JSON Intro
JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. [Read more](http://json.org/). JSON supports **String, Array, Object, Boolean, Float, Null, Decimal** and more.

## The MongoDB Query Language + ATLAS - Loading Database
- First we connected to an ATLAS cluster using an unique command through the CMD shell.
- We can switch databases in the cluster using this : `use <database-name>`
- We can view the collections in that database using : `show collections`
- We can see the documents in the collection using : `db.<collection-name>.find().pretty()`
- We then connected the Sandbox cluster of ATLAS on to the Mongo Shell by pasting the link and entering the password for the cluster **m001-students**
- You should end up getting connected to the **primary** cluster.
- In case you get connected to a Secondary node, in order to run queries or commands, you will have to run `rs.slaveOk()` shell command, to list databases, collections and run queries.
- Show which databases are currently present in the cluster using `show dbs`
- We now opened the path where the dataset is present, open the Mongo Shell and connect it using the above steps, then use `load("<dataset-name.js>")`. It should return **true** for successful loading of data. You can check it using `show dbs`.

##### Connect Sandbox to your Compass
- Open Compass
- New Connection
- Open browser and find the name of the primary cluster and enter it to the **hostname** on Compass
- Port name use usually default to 27017
- Set the username and password associated with the cluster
- Now you can normally navigate through the databases and collections

##### Creating Documents

**insertOne()** <br>
- Click on the database from left panel
- Go to **Create Collection**. This creates an empty collection. Now we add documents to it.
- Now navigate into the new collection and press upon **Insert Document**. Then enter the data and their data types with the simple UI, you can also edit.
- How to do the same from **MONGO SHELL**. Let's see.
- Navigate to the database using `use <db-name>` 
- Check the collections present using `show collections`
- Recheck the database using command `db`
- Use this to insert one entry: `db.<collection-name>.insertOne({fieldname: value,..})`
- Example : `db.movieScratch.insertOne({title: "Star Trek II", year: 1982, imdb: "tt0084726"})`
- It would return another document that would say whether the request is **acknowledged** and the **Object's Insert ID** <br>

By default if we do not assign an ID (*_id*) to the new entry then MongoDB will assign an unique ObjectID on its own. But we can supply one like this as well `db.movieScratch.insertOne({_id:"tt0084726", title: "Star Trek II", year: 1982, imdb: "tt0084726"})`. Now in the compass, we can see two different documents having the same data but with different ID. So the ID differentiates them. <br>

**insertMany()**<br>
In this case we pass an array of documents. This command does an ordered insert. When it finds an error/duplicate, it terminates. If we dont want that, we can pass a second argument to insertMany() which is `{"ordered": false}`. You can refer to the ZIP folder handouts and open the javascript files *creating_documents*, using a text editor to view the full query.

##### Reading Documents
- Selectors in a MongoDB filter are **AND**ed together. For example: Filter: {condtion1, condition2}, refers to that the two conditions both has to be satisfied.
- On compass we write a filter as, for example: `{mpaaRating: "PG-13", year: 2009}`
- The same operation can be performed on a MongoShell using the **find()** function/command.
- We do: `db.movies.find({mpaaRating: "PG-13", year: 2009}).pretty()`. Note that pretty() will just bring the output to a good readableeasy format, that's all it does.
- These are also known as **equality filters**.
- We can access nested field or inner fields using got notations.
- To count the number of output rows returned use **.count()**. Example: `db.movieDetails.find({"awards.nominations":2, "awards.wins":2}).count()`

##### Reading On Array Fields
- `{"writers.0":"Ethan Coen", "writers.1":"Joel Coen"}` Contain Ethan and Joel as the lead/only two writers.
- `{writers:["Ethan Coen","Joel Coen"]}` Contain Ethan and Joel as writers
- `{cast:"Daniel Radcliff"}` Daniel Radcliff is a part of the cast
- `{"cast.0":"Daniel Radcliff"}` Daniel Radcliff is the first name in the cast, i.e. he is the lead starring role

##### Cursors
Normally when we pass a filter/query to the Mongo Shell, it returns 20 matches at most. It is know as the **cursor**. We can iterate the cursor using **it** keyword for iterate, to fetch more matching results.
##### Projections
Normally a result will show all the fields of the matching documents. We can restrict this using a projection, *which is the second parameter to find()*.
Example: `db.movieDetails.find({rated:"PG-13"},{title:1, _id:0})`
##### UpdateOne Entry In Document
This will update the first matching result only. Example:
`db.<collection-name>.updateOne({
	<filter>
	}, 
	{
	$set: {
		<new field>:<new value> or <old field>:<new value>
	}
})`
Example:
`db.movieDetails.updateOne({
	title:"The Martian"
	},
	{
		$set: {
			poster:"http://...some image poster link"
		}
	}
)`

##### Other Similar Update Operators
- $set : Sets the value of a field in a doc
- $unset : Remove the specified field
- $min : Only updates the field if the specified value is less than the existing field value.
- $max : Only updates the field if the specified value is greater than the existing field value
-rename : Renames the field
- [See more here](https://docs.mongodb.com/manual/reference/operator/update/index.html)
- [See this too](https://www.youtube.com/watch?v=9k6upO70NLI)

##### Updating several rows using updateMany()
`db.movieDetails.updateOne({
	rated: null
	},
	{
		$unset: {
			rated: ""
		}
	}
)`
The rules are same as updateOne() but this updates all matching documents simultaneously.

##### Upsert
Update a matching document. That is update documents matching the filter, if there are none : insert the document as a new document in the collection.[Reference](https://www.youtube.com/watch?v=r2eg0F49SuQ).

##### Update - Replace One
- [Reference lecture](https://www.youtube.com/watch?v=4d_XbnX67oc)
- [Reference notes](https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/index.html)

##### Delete Operations
**Delete One Document**
`db.reviews.deleteOne({_id: ObjectId("5bb331d5ef07665af3db94b7")})`
Returns : *{ "acknowledged" : true, "deletedCount" : 1 }*
**Delete Many Documents**
`db.reviews.deleteMany({reviewer_id: 759723314})`
Returns : *{ "acknowledged" : true, "deletedCount" : 3 }*

Extra Question : [What is sharding? Why important?](https://stackoverflow.com/questions/992988/what-is-sharding-and-why-is-it-important)


## Deeper Dive - MongoDB Query Language
**Query Selector Operators [here](https://docs.mongodb.com/manual/reference/operator/query/)**

- db.movieDetails.find({runtime: {$gt: 90}})
- db.movieDetails.find({runtime: {$gt: 90}}, {_id: 0, title: 1, runtime: 1})
- db.movieDetails.find({runtime: {$gt: 90, $lt: 120}}, {_id: 0, title: 1, runtime: 1})
- db.movieDetails.find({runtime: {$gte: 90, $lte: 120}}, {_id: 0, title: 1, runtime: 1})
- db.movieDetails.find({runtime: {$gte: 180}, "tomato.meter": 100}, {_id: 0, title: 1, runtime: 1})
- db.movieDetails.find({rated: {$ne: "UNRATED"}}, {_id: 0, title: 1, rated: 1})
- db.movieDetails.find({rated: {$in: ["G", "PG"]}}, {_id: 0, title: 1, rated: 1})
- db.movieDetails.find({rated: {$in: ["G", "PG", "PG-13"]}}, {_id: 0, title: 1, rated: 1}).pretty()
- db.movieDetails.find({rated: {$in: ["R", "PG-13"]}}, {_id: 0, title: 1, rated: 1}).pretty()


- db.moviesDetails.find({mpaaRating: {$exists: true}})

- db.moviesDetails.find({mpaaRating: {$exists: false}})

- db.movieDetails.find({mpaaRating: null})

- db.movieDetails.find({})

- db.movies.find({viewerRating: {$type: "int"}}).pretty()

- db.movies.find({viewerRating: {$type: "double"}}).pretty()

- db.movieDetails.find({$or: [{"tomato.meter": {$gt: 95}},                               
                            {"metacritic": {$gt: 88}}]},
                     {_id: 0, title: 1, "tomato.meter": 1, "metacritic": 1})

- db.movieDetails.find({$and: [{"tomato.meter": {$gt: 95}},                               
                            {"metacritic": {$gt: 88}}]},
                     {_id: 0, title: 1, "tomato.meter": 1, "metacritic": 1})

- db.movieDetails.find({"tomato.meter": {$gt: 95},                               
                      "metacritic": {$gt: 88}},
                     {_id: 0, title: 1, "tomato.meter": 1, "metacritic": 1})

- db.movieDetails.find({$and: [{"metacritic": {$ne: null}},
                             {"metacritic": {$exists: true}}]},
                          {_id: 0, title: 1, "metacritic": 1})

- db.movieDetails.find({$and: [{"metacritic": null},
                             {"metacritic": {$exists: true}}]},
                     {_id: 0, title: 1, "metacritic": 1})


For more such queries check the week-3 handouts ZIP or documentations.
